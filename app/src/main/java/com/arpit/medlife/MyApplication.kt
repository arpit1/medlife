package com.arpit.medlife

import android.app.Application
import com.arpit.medlife.data.db.AppDatabase
import com.arpit.medlife.data.db.repository.MoviesRoomDbRepository
import com.arpit.medlife.data.preferences.PreferenceProvider
import com.arpit.medlife.network.MyApi
import com.arpit.medlife.network.NetworkConnectionInterceptor
import com.arpit.medlife.repository.GetHomeDataRepository
import com.arpit.medlife.ui.datasource.MoviesDataSourceFactory
import com.arpit.medlife.ui.detail.MovieDetailViewModelFactory
import com.arpit.medlife.ui.home.HomeViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MyApplication: Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MyApplication))

        bind() from singleton { MyApi(instance()) }

        bind() from singleton { NetworkConnectionInterceptor(instance()) }

        bind() from singleton { AppDatabase(instance()) }

        bind() from singleton { MoviesRoomDbRepository(instance()) }

        bind() from singleton { PreferenceProvider(instance()) }

        bind() from singleton { GetHomeDataRepository(instance()) }

        bind() from provider { MoviesDataSourceFactory(instance()) }

        bind() from provider { HomeViewModelFactory(instance(), instance(), instance()) }

        bind() from provider { MovieDetailViewModelFactory(instance()) }
    }
}