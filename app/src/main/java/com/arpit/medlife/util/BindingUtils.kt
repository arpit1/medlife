package com.arpit.medlife.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.arpit.medlife.R
import com.squareup.picasso.Picasso

object BindingUtil {

    @BindingAdapter("imageUrl", "errorDrawable")
    @JvmStatic
    fun setImage(imageView: ImageView, url: String?, errorDrawable: Drawable?) {
        var errorDrawable = errorDrawable

        if (url == null || url.isEmpty()) {
            return
        }

        if (errorDrawable == null) {
            errorDrawable =
                ContextCompat.getDrawable(
                    imageView.context,
                    R.drawable.ic_image_placeholder_wrapper
                )
        }

        errorDrawable?.let {
            Picasso.get().load(url)
                .placeholder(it).error(it).into(imageView)
        }
    }
}