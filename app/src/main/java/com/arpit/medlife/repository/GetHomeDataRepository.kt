package com.arpit.medlife.repository

import com.arpit.medlife.data.db.entities.Movies
import com.arpit.medlife.data.db.entities.SearchResult
import com.arpit.medlife.data.response.MovieDetail
import com.arpit.medlife.network.MyApi
import com.arpit.medlife.network.SafeApiRequest

class GetHomeDataRepository(
    private val api: MyApi
): SafeApiRequest() {

    suspend fun getMoviesList(value: String, pageCount: Int, requestedLoadSize: Int): SearchResult {
        return apiRequest { api.getMoviesData(value, "fe7b5d3e", pageCount) }
    }

    suspend fun getMovieDetail(value: String): MovieDetail {
        return apiRequest { api.getMovieDetail(value, "fe7b5d3e") }
    }
}