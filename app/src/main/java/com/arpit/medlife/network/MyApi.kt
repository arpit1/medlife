package com.arpit.medlife.network

import com.arpit.medlife.data.db.entities.SearchResult
import com.arpit.medlife.data.response.MovieDetail
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface MyApi {

    @GET(".")
    suspend fun getMoviesData(
        @Query("s") input: String,
        @Query("apikey") apikey: String,
        @Query("page") page: Int
    ): Response<SearchResult>

    @GET(".")
    suspend fun getMovieDetail(
        @Query("t") input: String,
        @Query("apikey") apikey: String
    ): Response<MovieDetail>

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): MyApi {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("https://www.omdbapi.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(MyApi::class.java)
        }
    }
}