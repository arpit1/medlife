package com.arpit.medlife.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arpit.medlife.data.db.repository.MoviesRoomDbRepository
import com.arpit.medlife.repository.GetHomeDataRepository
import com.arpit.medlife.ui.datasource.MoviesDataSourceFactory


class HomeViewModelFactory(
    private val getHomeDataRepository: GetHomeDataRepository,
    private val moviesDataSourceFactory: MoviesDataSourceFactory,
    private val moviesRoomDbRepository: MoviesRoomDbRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(getHomeDataRepository, moviesDataSourceFactory, moviesRoomDbRepository) as T
    }
}