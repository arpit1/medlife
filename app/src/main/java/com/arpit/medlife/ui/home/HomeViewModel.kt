package com.arpit.medlife.ui.home

import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.arpit.medlife.data.db.entities.Movies
import com.arpit.medlife.data.db.repository.MoviesRoomDbRepository
import com.arpit.medlife.data.model.ErrorActionListener
import com.arpit.medlife.data.model.ErrorModel
import com.arpit.medlife.network.mainScope
import com.arpit.medlife.repository.GetHomeDataRepository
import com.arpit.medlife.ui.State
import com.arpit.medlife.ui.datasource.MoviesDataSource
import com.arpit.medlife.ui.datasource.MoviesDataSourceFactory
import kotlinx.coroutines.launch

private const val TRIGGER_AUTO_COMPLETE = 100
private const val AUTO_COMPLETE_DELAY: Long = 300

class HomeViewModel(
    private val getHomeDataRepository: GetHomeDataRepository,
    private val moviesDataSourceFactory: MoviesDataSourceFactory,
    private val moviesRoomDbRepository: MoviesRoomDbRepository
) : ViewModel() {

    val errorModel: ErrorModel by lazy { ErrorModel() }
    val onError = ObservableBoolean(false)
    val clickLiveData = MutableLiveData<Movies>()
    val message = MutableLiveData<String>()
    val favoriteClicked = MutableLiveData<Int>()
    private var searchVal: String = ""
    private var listUpdated = false
    var moviesList: LiveData<PagedList<Movies>>
    private val pageSize = 5

    init {
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize * 2)
            .setEnablePlaceholders(false)
            .build()
        moviesList = LivePagedListBuilder(
            moviesDataSourceFactory,
            config
        ).build()
    }

    fun getState(): LiveData<State> = Transformations.switchMap(
        moviesDataSourceFactory.moviesDataSourceLiveData,
        MoviesDataSource::state
    )

    fun cardClick(data: Movies) {
        clickLiveData.value = data
    }

    fun onLikedClick(data: Movies) {
        var pos = -1
        for (item in moviesList.value!!) {
            pos++
            if (data.imdbID == item.imdbID) {
                break
            }
        }
        data.isFavorite = !data.isFavorite
        updateDb(data, pos)
    }

    private fun updateDb(data: Movies, pos: Int) {
        if (data.isFavorite) {
            arrLikedMovieIds.add(data.imdbID)
            mainScope.launch {
                moviesRoomDbRepository.saveMovie(data)
            }
        } else {
            arrLikedMovieIds.remove(data.imdbID)
            mainScope.launch {
                moviesRoomDbRepository.deleteMovie(data.imdbID)
            }
        }
        favoriteClicked.postValue(pos)
    }

    fun showErrorModel(
        title: String,
        subTitle: String
    ) {
        errorModel.apply {
            errorTitle = title
            errorSubTitle = subTitle
            buttonText = RETRY
            errorActionListener = object : ErrorActionListener {
                override fun onErrorActionClicked() {
                    moviesList.value?.dataSource?.invalidate()
                }
            }
        }
    }

    fun onTextChanged(
        s: CharSequence,
        start: Int,
        before: Int,
        count: Int
    ) {
        Log.w("tag", "onTextChanged $s")
        searchVal = s.toString()
        handler.removeMessages(TRIGGER_AUTO_COMPLETE)
        handler.sendEmptyMessageDelayed(TRIGGER_AUTO_COMPLETE, AUTO_COMPLETE_DELAY)
    }

    private val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: android.os.Message?) {
            if (msg?.what == TRIGGER_AUTO_COMPLETE) {
                if (!TextUtils.isEmpty(searchVal.trim()) && searchVal.trim().length >= 3) {
                    listUpdated = true
                    SEARCH_VAL = searchVal
                    moviesList.value?.dataSource?.invalidate()
                } else {
                    message.postValue("LENGTH")
                    if (listUpdated) {
                        listUpdated = false
                        SEARCH_VAL = "GAME"
                        moviesList.value?.dataSource?.invalidate()
                    }
                }
            }
        }
    }

    fun listIsEmpty(): Boolean {
        return moviesList.value?.isEmpty() ?: true
    }

    companion object {
        const val ERROR_TITLE = "Something went wrong.."
        const val ERROR_DESCRIPTION = "An alien is probably blocking your signal."
        const val RETRY = "RETRY"
        var SEARCH_VAL = "GAME"
        var arrLikedMovieIds = ArrayList<String>()
    }
}