package com.arpit.medlife.ui.detail

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.arpit.medlife.R
import com.arpit.medlife.databinding.ActivityMovieDetailBinding
import com.arpit.medlife.network.mainScope
import com.arpit.medlife.ui.adapter.RatingListAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.activity_movie_detail.progressBar
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MovieDetailActivity: AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private lateinit var viewModel: MovieDetailViewModel

    private val factory: MovieDetailViewModelFactory by instance()

    private lateinit var toolbar: Toolbar
    private lateinit var binding: ActivityMovieDetailBinding

    private lateinit var ratingListAdapter : RatingListAdapter

    @Override
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
    }

    private fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_movie_detail)

        viewModel = ViewModelProvider(this, factory).get(MovieDetailViewModel::class.java)
        binding.viewmodel = viewModel

        viewModel.fetchMoviesDetails(intent.getStringExtra("name"))

        setToolbar()
        setObserver()
    }

    private fun initRecyclerView() {
        ratingListAdapter = RatingListAdapter(arrayListOf())
        ratings.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = ratingListAdapter
        }
    }

    private fun setObserver() {
        viewModel.onFailure.observe(this, Observer {
            progressBar.visibility = View.GONE
            mainScope.launch {
                binding.errorModel = viewModel.errorModel
            }
        })

        viewModel.onStart.observe(this, Observer {
            progressBar.visibility = View.VISIBLE
            viewModel.onError.set(false)
        })

        viewModel.onSuccess.observe(this, Observer {
            progressBar.visibility = View.GONE
            content.visibility = View.VISIBLE
            initRecyclerView()
            mainScope.launch {
                binding.data = it
            }
            ratingListAdapter.submitRating(ArrayList(it.Ratings!!))
            ratingListAdapter.notifyDataSetChanged()
            viewModel.onError.set(false)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) onBackPressed()
        return super.onOptionsItemSelected(item!!)
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Movies Detail"
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        toolbar.setNavigationIcon(R.drawable.back_arrow_black)
        setSupportActionBar(toolbar)
    }
}