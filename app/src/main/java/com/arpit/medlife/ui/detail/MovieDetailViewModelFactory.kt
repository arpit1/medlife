package com.arpit.medlife.ui.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.arpit.medlife.repository.GetHomeDataRepository

class MovieDetailViewModelFactory(
    private val getHomeDataRepository: GetHomeDataRepository
) : ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MovieDetailViewModel(getHomeDataRepository) as T
    }
}