package com.arpit.medlife.ui.detail

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.arpit.medlife.data.model.ErrorActionListener
import com.arpit.medlife.data.model.ErrorModel
import com.arpit.medlife.data.response.MovieDetail
import com.arpit.medlife.network.mainScope
import com.arpit.medlife.repository.GetHomeDataRepository
import com.arpit.medlife.ui.home.HomeViewModel
import com.arpit.medlife.util.ApiException
import com.arpit.medlife.util.NoInternetException
import kotlinx.coroutines.launch

class MovieDetailViewModel(
    private val getHomeDataRepository: GetHomeDataRepository
) : ViewModel() {

    val errorModel: ErrorModel by lazy { ErrorModel() }
    val onError = ObservableBoolean(false)
    val onStart = MutableLiveData<Any>()
    val onSuccess = MutableLiveData<MovieDetail>()
    val onFailure = MutableLiveData<String>()
    var movieTitle = ""

    fun fetchMoviesDetails(value: String) {
        movieTitle = value
        onStart.postValue("")
        onError.set(false)
        mainScope.launch {
            try {
                val response = getHomeDataRepository.getMovieDetail(value)
                response.let {
                    onError.set(false)
                    onSuccess.postValue(it)
                    return@launch
                }
            } catch (ex: ApiException) {
                onError.set(true)
                showErrorModel(
                    HomeViewModel.ERROR_TITLE,
                    HomeViewModel.ERROR_DESCRIPTION
                )
                onFailure.postValue("${ex.message}")
                ex.printStackTrace()
            } catch (ex: NoInternetException) {
                onError.set(true)
                showErrorModel(
                    HomeViewModel.ERROR_TITLE,
                    HomeViewModel.ERROR_DESCRIPTION
                )
                onFailure.postValue("${ex.message}")
                ex.printStackTrace()
            }
        }
    }

    private fun showErrorModel(
        title: String,
        subTitle: String
    ) {
        errorModel.apply {
            errorTitle = title
            errorSubTitle = subTitle
            buttonText = HomeViewModel.RETRY
            errorActionListener = object : ErrorActionListener {
                override fun onErrorActionClicked() {
                    fetchMoviesDetails(movieTitle)
                }
            }
        }
    }
}