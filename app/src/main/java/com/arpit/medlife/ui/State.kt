package com.arpit.medlife.ui

enum class State {
    DONE, LOADING, ERROR
}