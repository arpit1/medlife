package com.arpit.medlife.ui.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.arpit.medlife.data.db.entities.Movies
import com.arpit.medlife.repository.GetHomeDataRepository

class MoviesDataSourceFactory(
    private val getHomeDataRepository: GetHomeDataRepository
) : DataSource.Factory<Int, Movies>() {

    val moviesDataSourceLiveData = MutableLiveData<MoviesDataSource>()

    override fun create(): DataSource<Int, Movies> {
        val newsDataSource = MoviesDataSource(getHomeDataRepository)
        moviesDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}