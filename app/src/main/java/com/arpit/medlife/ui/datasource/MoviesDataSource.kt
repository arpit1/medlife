package com.arpit.medlife.ui.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.arpit.medlife.data.db.entities.Movies
import com.arpit.medlife.network.ioScope
import com.arpit.medlife.repository.GetHomeDataRepository
import com.arpit.medlife.ui.State
import com.arpit.medlife.ui.home.HomeViewModel.Companion.SEARCH_VAL
import com.arpit.medlife.util.ApiException
import com.arpit.medlife.util.NoInternetException
import kotlinx.coroutines.launch

class MoviesDataSource(
    private val repository: GetHomeDataRepository
) : PageKeyedDataSource<Int, Movies>() {

    var state: MutableLiveData<State> = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Movies>
    ) {
        updateState(State.LOADING)
        ioScope.launch {
            try {
                val response = repository.getMoviesList(SEARCH_VAL, 1, params.requestedLoadSize)
                response.let {
                    if (it.Response) {
                        updateState(State.DONE)
                        response.Search?.let { list ->
                            callback.onResult(
                                list,
                                null,
                                2
                            )
                        }
                    } else {
                        updateState(State.ERROR)
                    }
                    return@launch
                }
            } catch (ex: ApiException) {
                updateState(State.ERROR)
                ex.printStackTrace()
            } catch (ex: NoInternetException) {
                updateState(State.ERROR)
                ex.printStackTrace()
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movies>) {
        updateState(State.LOADING)

        ioScope.launch {
            try {
                val response =
                    repository.getMoviesList(SEARCH_VAL, params.key, params.requestedLoadSize)
                response.let {
                    if (it.Response) {
                        updateState(State.DONE)
                        response.Search?.let {
                            callback.onResult(
                                it,
                                params.key + 1
                            )
                        }
                    } else {
                        updateState(State.ERROR)
                    }
                    return@launch
                }
            } catch (ex: ApiException) {
                updateState(State.ERROR)
                ex.printStackTrace()
            } catch (ex: NoInternetException) {
                updateState(State.ERROR)
                ex.printStackTrace()
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movies>) {
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }
}