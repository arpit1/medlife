package com.arpit.medlife.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.arpit.medlife.R
import com.arpit.medlife.data.db.entities.Movies
import com.arpit.medlife.databinding.LayoutLikedItemBinding
import com.arpit.medlife.databinding.LayoutMovieItemBinding
import com.arpit.medlife.ui.home.HomeViewModel

class LikedMoviesListAdapter(
    var movieList: ArrayList<Movies>,
    private val model: HomeViewModel
) : RecyclerView.Adapter<LikedMoviesListAdapter.MovieAdapterViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapterViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutLikedItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.layout_liked_item, parent, false)
        return MovieAdapterViewHolder(binding.root, binding)
    }

    fun setMoviesList(list: ArrayList<Movies>) {
        movieList = list
    }

    override fun getItemCount() = movieList.size

    override fun onBindViewHolder(holder: MovieAdapterViewHolder, position: Int) {
        val movieResult: Movies = movieList[position]

        holder.binding.data = movieResult
        holder.binding.viewModel = model
    }

    class MovieAdapterViewHolder(itemView: View, var binding: LayoutLikedItemBinding) :
        RecyclerView.ViewHolder(itemView)
}