package com.arpit.medlife.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.arpit.medlife.R
import com.arpit.medlife.data.response.Rating
import com.arpit.medlife.databinding.LayoutRatingItemBinding

class RatingListAdapter(
    var ratingList: ArrayList<Rating>
) : RecyclerView.Adapter<RatingListAdapter.RatingAdapterViewHolder>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingAdapterViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutRatingItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.layout_rating_item, parent, false)
        return RatingAdapterViewHolder(binding.root, binding)
    }

    fun submitRating(ratingList: ArrayList<Rating>) {
        this.ratingList = ratingList
    }

    override fun getItemCount() = ratingList.size

    override fun onBindViewHolder(holder: RatingAdapterViewHolder, position: Int) {
        val rating: Rating = ratingList[position]

        holder.binding.data = rating
    }

    class RatingAdapterViewHolder(itemView: View, var binding: LayoutRatingItemBinding) :
        RecyclerView.ViewHolder(itemView)
}