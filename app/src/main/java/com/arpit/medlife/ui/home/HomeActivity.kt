package com.arpit.medlife.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.arpit.medlife.R
import com.arpit.medlife.data.db.repository.MoviesRoomDbRepository
import com.arpit.medlife.databinding.ActivityMainBinding
import com.arpit.medlife.network.mainScope
import com.arpit.medlife.ui.State.*
import com.arpit.medlife.ui.adapter.LikedMoviesListAdapter
import com.arpit.medlife.ui.adapter.MovieListAdapter
import com.arpit.medlife.ui.detail.MovieDetailActivity
import com.arpit.medlife.util.hideKeyboard
import com.arpit.medlife.util.showToast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class HomeActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private lateinit var viewModel: HomeViewModel

    private val factory: HomeViewModelFactory by instance()

    private val moviesRoomDbRepository: MoviesRoomDbRepository by instance()

    private lateinit var toolbar: Toolbar
    private lateinit var binding: ActivityMainBinding

    private lateinit var moviesListAdapter: MovieListAdapter
    private lateinit var likesMoviesListAdapter: LikedMoviesListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setBinding()
    }

    private fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewModel

        initRecyclerView()
        initState()
        setToolbar()
        setObserver()
        getLikedMovies()
    }

    private fun setObserver() {
        viewModel.message.observe(this, Observer {
            if (it == "LENGTH")
                showToast("Please type atleast 3 characters to search")
            else {
                showToast("No Result Found")
                search.hideKeyboard()
            }
        })

        viewModel.clickLiveData.observe(this, Observer {
            Intent(this, MovieDetailActivity::class.java).also { intent ->
                intent.putExtra("name", it.Title)
                startActivity(intent)
            }
        })

        viewModel.favoriteClicked.observe(this, Observer {
            moviesListAdapter.notifyItemChanged(it)
        })
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = "Movies List"
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.black))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        toolbar.navigationIcon = null
        setSupportActionBar(toolbar)
    }

    private fun initRecyclerView() {
        moviesListAdapter = MovieListAdapter(viewModel)
        movieListRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = moviesListAdapter
        }
        viewModel.moviesList.observe(this, Observer {
            moviesListAdapter.submitList(it)
        })
    }

    private fun getLikedMovies() {
        likesMoviesListAdapter = LikedMoviesListAdapter(arrayListOf(), viewModel)
        likedItems.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = likesMoviesListAdapter
        }
        mainScope.launch {
            moviesRoomDbRepository.getAllMovies().observe(this@HomeActivity, Observer {
                HomeViewModel.arrLikedMovieIds.clear()
                it.forEach {
                    HomeViewModel.arrLikedMovieIds.add(it.imdbID)
                }

                likesMoviesListAdapter.setMoviesList(ArrayList(it))
                likesMoviesListAdapter.notifyDataSetChanged()
            })
        }
    }

    private fun initState() {
        viewModel.getState().observe(this, Observer { state ->
            progressBar.visibility = if (viewModel.listIsEmpty() && state == LOADING) View.VISIBLE else View.GONE
            if (!viewModel.listIsEmpty()) {
                moviesListAdapter.setState(state ?: DONE)
            }

            when (state) {
                ERROR -> {
                    viewModel.onError.set(true)
                    viewModel.showErrorModel(
                        HomeViewModel.ERROR_TITLE,
                        HomeViewModel.ERROR_DESCRIPTION
                    )
                    mainScope.launch {
                        binding.errorModel = viewModel.errorModel
                    }
                }
                DONE -> {
                    viewModel.onError.set(false)
                }
                LOADING -> {
                    viewModel.onError.set(false)
                }
            }
        })
    }
}