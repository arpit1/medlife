package com.arpit.medlife.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.arpit.medlife.R
import com.arpit.medlife.data.db.entities.Movies
import com.arpit.medlife.databinding.LayoutMovieItemBinding
import com.arpit.medlife.ui.State
import com.arpit.medlife.ui.home.HomeViewModel

class MovieListAdapter(
    private val model: HomeViewModel
) : PagedListAdapter<Movies, MovieListAdapter.MovieAdapterViewHolder>(MoviesDiffCallback) {

    private lateinit var layoutInflater: LayoutInflater
    private var state = State.LOADING

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieAdapterViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        val binding: LayoutMovieItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.layout_movie_item, parent, false)

        binding.viewModel = model
        return MovieAdapterViewHolder(binding.root, binding)
    }

    companion object {
        val MoviesDiffCallback = object : DiffUtil.ItemCallback<Movies>() {
            override fun areItemsTheSame(oldItem: Movies, newItem: Movies): Boolean {
                return oldItem.Title == newItem.Title
            }

            override fun areContentsTheSame(oldItem: Movies, newItem: Movies): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onBindViewHolder(holder: MovieAdapterViewHolder, position: Int) {
        val movieResult: Movies? = getItem(position)
        movieResult?.let {
            it.isFavorite = HomeViewModel.arrLikedMovieIds.contains(it.imdbID)
        }
        holder.binding.data = movieResult
//        holder.binding.viewModel = model
    }

    fun setState(state: State) {
        this.state = state
        notifyItemChanged(super.getItemCount())
    }

    class MovieAdapterViewHolder(itemView: View, var binding: LayoutMovieItemBinding) :
        RecyclerView.ViewHolder(itemView)
}