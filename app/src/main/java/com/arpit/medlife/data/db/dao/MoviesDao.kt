package com.arpit.medlife.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.arpit.medlife.data.db.entities.Movies

@Dao
interface MoviesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(movie: Movies): Long

    @Query("DELETE FROM Movies WHERE imdbID = :id")
    suspend fun deleteMovie(id: String)

    @Query("SELECT * FROM Movies")
    fun getAllMovies(): LiveData<List<Movies>>
}