package com.arpit.medlife.data.model

import android.graphics.drawable.Drawable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.arpit.medlife.BR

class ErrorModel : BaseObservable() {

    @get:Bindable
    var errorTitle: String? = null
        set(errorTitle) {
            field = errorTitle
            notifyPropertyChanged(BR.errorTitle)
        }
    @get:Bindable
    var errorSubTitle: String? = null
        set(errorSubTitle) {
            field = errorSubTitle
            notifyPropertyChanged(BR.errorSubTitle)
        }
    var errorActionListener: ErrorActionListener? = null
    @get:Bindable
    var buttonText: String? = null
        set(buttonText) {
            field = buttonText
            notifyPropertyChanged(BR.buttonText)
        }
}