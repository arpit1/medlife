package com.arpit.medlife.data.db.repository

import com.arpit.medlife.data.db.AppDatabase
import com.arpit.medlife.data.db.entities.Movies

class MoviesRoomDbRepository(private val database: AppDatabase) {
    suspend fun saveMovie(movie: Movies) = database.getMoviesDao().upsert(movie)

    fun getAllMovies() = database.getMoviesDao().getAllMovies()

    suspend fun deleteMovie(id: String) = database.getMoviesDao().deleteMovie(id)
}