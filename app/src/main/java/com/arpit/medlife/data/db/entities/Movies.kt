package com.arpit.medlife.data.db.entities

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

data class SearchResult(
    var Search: List<Movies>? = null,
    var totalResults: String? = null,
    var Response: Boolean = false
)

@Entity
data class Movies(
    @NonNull
    @PrimaryKey
    var imdbID: String,
    var Title: String? = null,
    var Year: String? = null,
    var Type: String? = null,
    var Poster: String? = null,
    var isFavorite: Boolean = false
)