package com.arpit.medlife.data.model

interface ErrorActionListener {
    fun onErrorActionClicked()
}