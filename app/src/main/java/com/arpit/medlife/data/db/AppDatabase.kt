package com.arpit.medlife.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.arpit.medlife.data.db.dao.MoviesDao
import com.arpit.medlife.data.db.entities.Movies

@Database(
    entities = [Movies::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getMoviesDao(): MoviesDao

    companion object {
        //    @Volatile is used so that this variable is immediately visible to all other threads
        @Volatile
        private var instance: AppDatabase? = null
        //    it is use to check that two instances not get created for database
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also {
                instance = it
            }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                AppDatabase::class.java,
                "Medlife.db"
            ).build()
    }
}